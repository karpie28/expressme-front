/**
*
* FriendBox
*
*/

import React from 'react';
import style from "./style.less";
import Button from "components/Button";
import Dropzone from "react-dropzone";
import * as emotions from "images/emotions";
import { api} from "api/Sagas";
import moment from "moment";

const emotionsImages = Object.values(emotions);
const emotionsNames = [
  "Angry",
  "Sad",
  "Neutral",
  "Happy",
  "Excited"
];


class FriendBox extends React.Component { // eslint-disable-line react/prefer-stateless-function
  state = {
    visible: false,
    friendRemoved: false
  };

  componentDidMount() {
    setTimeout(() => this.setState({ visible: true }), 100);
  }

  deleteFriend() {
    const { user_id_inviting, name } = this.props;
    if (confirm(`Do you really want to remove ${name} from your friends?`)) {
      this.props.deleteFriend(user_id_inviting);
      this.setState({ friendRemoved: true });
    }
  }

  addFriend() {
    const { id, name } = this.props;
    api.invite(id)
      .then(request => alert(request.data.message || request.data.error))
      .catch(() => alert("There was an error while sending invitation. Please try again"));
  }

  render() {
    const { visible, friendRemoved } = this.state;
    const { avatar, name, surname, last_emotion, birth_date, created_at, delay } = this.props;

    const classes = [
      style.profile,
      visible && style["profile--visible"],
      friendRemoved && style["profile--removed"]
    ].filter(Boolean).join(" ");

    const emotionName = emotionsNames[last_emotion ? last_emotion - 1 : 2];
    const emotionImage = emotionsImages[last_emotion ? last_emotion - 1 : 2];

    return (
      <div className={classes} style={{transitionDelay: `${delay * 100}ms`}}>
        <div className={style.profile__avatar} style={{backgroundImage: `url('${avatar || "https://www.cockerandcarr.co.uk/wp-content/uploads/Person-placeholder.jpg"}')`}} />
        <div className={style.profile__content}>
          <h3>{name} {surname}</h3>
          <p>Birth date: <span>{ moment(birth_date).format("l") }</span></p>
          <p>Join date: <span>{ moment(created_at).format("l") }</span></p>
        </div>
        { this.props.deleteFriend &&
          <Button styling="yellow" href={() => this.deleteFriend()} className={style.button}>Remove</Button>
        }
        { this.props.addFriend &&
          <Button styling="yellow" href={() => this.addFriend()} className={style.button}>Invite</Button>
        }
        <div className={style.profile__emotion}>
          <img src={emotionImage} alt={emotionName} title={emotionName} />
        </div>
      </div>
    );
  }
}

FriendBox.propTypes = {

};

export default FriendBox;
