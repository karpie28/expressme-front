/**
*
* Chat
*
*/

import React from "react";
import { deleteFriendRequest, getFriendsRequest } from "api/actions/FriendActions";
import { connect } from "react-redux";
import { compose } from "redux";
import Pusher from "pusher-js";
import Scrollbars from "react-custom-scrollbars";
import { api } from "api/Sagas";
import style from "./style.less";
import * as emotions from "images/emotions";
import { emotion3 } from "images/emotions";
const emotionsImages = Object.values(emotions);

class Chat extends React.Component { // eslint-disable-line react/prefer-stateless-function

  state = {
    chats: [],
    value: ""
  };

  chats = {};

  componentDidMount() {
    const { id } = this.props.user.data;
    //Pusher.logToConsole = true;
    this.pusher = new Pusher(process.env.PUSHER_KEY, {
      cluster: "eu",
      forceTLS: true,
      authEndpoint: `${process.env.API_URL}/authorize`,
      auth: {
        headers: {
          Authorization: `Bearer ${api.getToken()}`
        }
      }
    });

    this.mainChannel = this.pusher.subscribe(`private-user-${id}`);
    this.mainChannel.bind("App\\Events\\NewMessage", this.handleNewChannel);

    this.props.getFriends();
  }

  componentWillUnmount() {
    this.mainChannel.unbind();
    this.pusher.disconnect();
  }

  handleNewMessage = (data) => {
    const { user, message, userToSent } = data;

    const userID = user.id === this.props.user.data.id ? userToSent : user.id;
    if (!this.chats[userID]) {
      this.chats[userID] = [];
    }
    this.chats[userID].push(message);
    this.forceUpdate();
    const currentChat = this.state.chats.find((item) => item.id === parseInt(userID));
    currentChat.scroll && currentChat.scroll.scrollToBottom();
  };

  handleNewChannel = (data) => {
    this.openChat(data.userWhoSent);
  };

  async openChat(user) {
    const { chats } = this.state;
    const { id } = this.props.user.data;
    const currentChatId = chats.findIndex((item) => item.id === user.id);
    if (currentChatId !== -1) {
      return this.toggleChat(currentChatId, false);
    }
    const response = await api.getChat(user.id);
    if (response.ok) {
      this.chats[user.id] = response.data;
    }

    const channel = this.pusher.subscribe(`private-chat-${Math.min(id, user.id)}-${Math.max(id, user.id)}`);
    channel.bind("App\\Events\\MessageSent", this.handleNewMessage);
    const chatId = chats.push({ ...user, channel, active: true, limit: 10, offset: 0, loading: false });

    this.setState({ chats }, () => {
      this.toggleChat(chatId, true);
    });
  }

  toggleChat(index, forcedValue = null) {
    const { chats } = this.state;
    const chat = chats[index];
    if (!chat) {
      return;
    }
    chat.active = forcedValue === null ? !chat.active : forcedValue;
    chats[index] = chat;
    this.setState({ chats }, () => chat.scroll && chat.scroll.scrollToBottom());
  }

  handleSubmit = async (e, index) => {
    if (e.keyCode !== 13 || e.shiftKey) {
      return;
    }
    const chat = this.state.chats[index];
    if (!chat) {
      return;
    }
    e.preventDefault();

    const { value } = e.target;
    e.target.value = "";
    const response = await api.postChat(chat.id, value);
    if (!response.ok) {
      e.target.value = value;
    }
  };

  handleScroll = async (index, chat, { top, scrollTop, scrollHeight }) => {
    if (!chat.loading && top <= 0.05) {
      chat.loading = true;
      chat.offset += chat.limit;

      const response = await api.getChat(chat.id, chat.limit, chat.offset);
      if (response.ok) {
        chat.loading = false;
        if (response.data.length === 0) {
          return;
        }
        this.chats[chat.id] = response.data.concat(this.chats[chat.id]);
        this.forceUpdate(() => chat.scroll.scrollTop(chat.scroll.getScrollHeight() - scrollHeight));
      }
    }
  };

  render() {
    const { friend, user } = this.props;
    const { chats, value } = this.state;
    const this_user_id = user.data.id;

    return (
      <div>
        <div className={style.friends}>
          { friend.list.map((item, index) => {
            const avatar = item.avatar || "https://www.cockerandcarr.co.uk/wp-content/uploads/Person-placeholder.jpg";
            return (
              <a className={style.friend} href="#" onClick={(e) => { e.preventDefault(); this.openChat(item); }}>
                <span>{item.name} {item.surname}</span>
                <img src={avatar} />
              </a>
            );
          }) }
        </div>
        <div className={style.chats}>
          { chats.map((chat, index) => {
            const messages = this.chats[chat.id];
            const receiverMessages = messages.filter(item => item.user_sent_id === this_user_id);
            const emotionFromList = receiverMessages.length > 0 && receiverMessages[receiverMessages.length - 1].emotion - 1;
            const emotion = isNaN(emotionFromList) ? (isNaN(chat.last_emotion) ? 2 : chat.last_emotion) : emotionFromList;
            const classes = [
              style.chat,
              chat.active && style["chat--active"]
            ].filter(Boolean).join(" ");

            return (
              <div key={index} className={classes}>
                <a href="#" className={style.chat__label} onClick={(e) => { e.preventDefault(); this.toggleChat(index); }}>
                  { chat.name } { chat.surname } <img src={emotionsImages[emotion] || emotion3} />
                </a>
                <div className={style.chat__content}>
                  <div className={style.messages} ref={ref => ref && (ref.scrollTop = ref.scrollHeight)}>
                    <Scrollbars
                      className={style.messages__scroll}
                      ref={ref => chat.scroll = ref} onScrollFrame={values => this.handleScroll(index, chat, values)}
                    >
                      <div className={style.messages__list}>
                        {
                          messages.map((message, n) => {
                            const classes = [
                              style.message,
                              message.user_sent_id !== this_user_id && style["message--sender"]
                            ].filter(Boolean).join(" ");

                            return <div key={n} className={classes}><span>{ message.text }</span></div>;
                          })
                        }
                      </div>
                    </Scrollbars>
                  </div>
                  <textarea
                    className={style.chat__input}
                    placeholder={`Type in your message for ${chat.name} and press Enter to send it.`}
                    data-onChange={(e) => this.setState({ value: e.target.value })}
                    onKeyDown={(e) => this.handleSubmit(e, index)}
                    data-value={value}
                  />
                </div>
              </div>
            );
          }) }
        </div>
      </div>
    );
  }
}

Chat.propTypes = {

};

function mapDispatchToProps(dispatch) {
  return {
    getFriends: () => dispatch(getFriendsRequest()),
    deleteFriend: (friendId) => dispatch(deleteFriendRequest({ friendId }))
  };
}

function mapStateToProps(mapState) {
  const state = mapState.toJS();
  const { friend, user } = state.api;
  return {
    friend,
    user
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export default compose(
  withConnect,
)(Chat);

