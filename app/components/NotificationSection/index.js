/**
*
* NotificationSection
*
*/

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { compose } from "redux";
import { api } from "api/Sagas";
import style from "./style.less";
import { getFriendsRequest, getInvitationsRequest } from "api/actions/FriendActions";
import Button from "components/Button";


class NotificationSection extends React.Component { // eslint-disable-line react/prefer-stateless-function

  static defaultProps = {
  };

  state = {
    invitations: []
  };

  componentDidMount() {
    this.props.getInvitations();
  }

  async accept(id) {
    const response = await api.acceptInvitation(id);
    if (response.ok) {
      alert("Hooray! You got a new friend!");
    } else {
      alert("An error occurred. Please try again");
    }
    this.props.getInvitations();
    this.props.getFriends();
  }

  render() {
    const { invitations, fetching } = this.props.friend;
    const list = invitations.filter(invitation => invitation.status === "waiting");
    return (
      <div className={style.box}>
        <h3>Invitations</h3>
        {
          list.length === 0 && <span>You don't have any new invitations.</span>
        }
        {
          list.map(invitation => (
            <div className={style.invitation}>
              <span>{ invitation.user.name } { invitation.user.surname }</span>
              <Button noMargin href={() => this.accept(invitation.id)}>✓</Button>
              <Button noMargin href={() => this.reject(invitation.id)}>✘</Button>
            </div>
          ))
        }
      </div>
    );
  }
}

NotificationSection.propTypes = {
  type: PropTypes.string.isRequired
};

function mapStateToProps(mapState) {
  const state = mapState.toJS();
  const { friend } = state.api;
  return {
    friend
  };
}

function mapDispatchToProps(dispatch) {
  return {
    getInvitations: () => dispatch(getInvitationsRequest()),
    getFriends: () => dispatch(getFriendsRequest())
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export default compose(
  withConnect,
)(NotificationSection);
