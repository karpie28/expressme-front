/**
*
* Feed
*
*/

import React from "react";
import PropTypes from "prop-types";
import * as emotions from "images/emotions";
import moment from "moment";
import style from "./style.less";
import { api } from "api/Sagas";
import TextareaAutosize from "react-autosize-textarea";

const emotionsImages = Object.values(emotions);
const emotionsNames = [
  "Angry",
  "Sad",
  "Neutral",
  "Happy",
  "Excited"
];

class Feed extends React.Component { // eslint-disable-line react/prefer-stateless-function
  state = {
    visible: false,
    height: null,
    liked: false,
    showComments: false,
    myComment: ""
  };

  myComments = [];
  reference = null;
  likeSent = false;
  likeDownloaded = false;

  isInViewport() {
    if (!this.reference) return false;
    const box = this.reference.getBoundingClientRect();
    return box.top + box.height >= 0 && box.bottom <= window.innerHeight + box.height;
  }

  componentDidMount() {
    this.setState({
      visible: this.isInViewport(),
      height: `${this.reference.offsetHeight}px`
    });
  }

  async componentWillReceiveProps(props) {
    const state = {};
    if (props.fixedVisibility) {
      return;
    }

    if (props.scrollTop !== this.props.scrollTop) {
      const { visible: oldVisible } = this.state;
      const visible = this.isInViewport();
      if (oldVisible !== visible) {
        state.visible = visible;
      }

      if (visible && !this.likeDownloaded) {
        this.likeDownloaded = true;
        const { feed, user_id } = this.props;
        const result = await api.getLike(feed.feed_id);
        if (result.ok && result.data.length > 0 && result.data.some(item => item.user_id === user_id)) {
          state.liked = true;
        }
      }
    }

    if (props.containerWidth !== this.props.containerWidth && this.reference) {
      state.height = `${this.reference.offsetHeight}px`;
    }

    this.setState(state);
  }

  handleLike = async (e) => {
    e.preventDefault();
    const { feed_id } = this.props.feed;
    const response = await api.postLike(feed_id);
    if (response.ok) {
      this.likeSent = true;
      this.setState({ liked: true });
    }
  };

  handleComments = async (e) => {
    e.preventDefault();
    this.setState({ showComments: !this.state.showComments });
    //const { feed_id } = this.props.feed;

    //const response = await api.getComments(feed_id);
    //console.log(response);
  };

  handleKey = async (e) => {
    const { myComment } = this.state;
    const { user_name, feed } = this.props;
    if(e.keyCode === 13 && !e.shiftKey) {
      e.preventDefault();
      if (myComment) {
        const response = await api.postComment(feed.feed_id, myComment);
        if(response.ok) {
          this.myComments.push({
            full_name: user_name,
            description: myComment,
            created_at: new Date()
          });
          this.setState({ myComment: "" });
        }
      }
    }
  };

  render() {
    const {
      visible,
      height,
      liked,
      myComment,
      showComments
    } = this.state;

    const {
      feed,
      fixedVisibility
    } = this.props;

    const {
      description,
      user_id,
      user,
      image,
      width: imageWidth,
      height: imageHeight,
      emotion,
      created_at,
      likes_count = 0
    } = feed;

    const comments = (feed.comments || []).concat(this.myComments);

    const emotionName = emotionsNames[emotion - 1];
    const emotionImage = emotionsImages[emotion - 1];
    const hasHeight = !visible && height !== null;

    const classes = [
      style.feed,
      image && style["feed--hasImage"],
      description && style["feed--hasDescription"],
      !image && typeof description === "string" && description.length <= 80 && style["feed--bigDescription"],
      (fixedVisibility || visible) && style["feed--visible"]
    ].filter(Boolean).join(" ");

    const likes = likes_count + (this.likeSent && liked ? 1 : 0);

    return (
      <div
        className={classes}
        ref={ref => this.reference = ref}
        style={!fixedVisibility && hasHeight ? { height } : {}}
      >
        { (fixedVisibility || !hasHeight) &&
          <div>
            <div className={style.feed__header}>
              <span className={style.author}>
                <img className={style.author__avatar} src={user.avatar || "https://www.cockerandcarr.co.uk/wp-content/uploads/Person-placeholder.jpg"} />
                <span className={style.author__name}>{ user.name } { user.surname }</span>
              </span>
              <time
                className={style.feed__time}
                dateTime={created_at.toISOString()}
                title={created_at.toLocaleString()}
              >
                { moment(created_at).fromNow() }
              </time>
            </div>
            <div className={style.feed__content}>
              { description &&
                <div className={style.feed__description}>
                  { description }
                </div>
              }
              { image &&
                <a className={style.feed__image}>
                   <img src={image} style={{maxWidth: imageWidth, maxHeight: imageHeight}} />
                </a>
              } {/* TODO: implement lightbox and width/height from server */}
            </div>
            <div className={style.feed__socials}>
              <a href="#" onClick={this.handleLike} className={liked && style.feed__liked}>{likes} Like{likes === 1 ? "" : "s"}</a>
              <a href="#" onClick={this.handleComments}>{comments.length} Comment{comments.length === 1 ? "" : "s"}</a>
            </div>
            <div className={style.feed__emotion}>
              <img src={emotionImage} alt={emotionName} title={emotionName} />
            </div>
            <div className={[style.feed__comments, showComments && style["feed__comments--visible"]].filter(Boolean).join(" ")}>
              { comments.length > 0 &&
                <ul className={style.feed__list}>
                  {comments.map(item => {
                    const commented_at = typeof item.created_at === "string" ? new Date(item.created_at.replace(" ", "T")) : item.created_at;

                    return (
                      <li>
                        <div>
                          <span>{item.full_name}</span>
                          <time
                            className={style.feed__time}
                            dateTime={commented_at.toISOString()}
                            title={commented_at.toLocaleString()}
                          >
                            { moment(commented_at).fromNow() }
                          </time>
                        </div>
                        <p>{item.description}</p>
                      </li>
                    );
                  })}
                </ul>
              }
              <TextareaAutosize
                className={style.feed__commentInput}
                placeholder="Type in your comment and press Enter."
                onKeyDown={this.handleKey}
                value={myComment}
                onChange={e => this.setState({ myComment: e.target.value })}
              />
            </div>
          </div>
        }
      </div>
    );
  }
}

Feed.propTypes = {
  feed: PropTypes.shape({
    description: PropTypes.string,
    user_id: PropTypes.number,
    image: PropTypes.string,
    full_name: PropTypes.string,
    emotion: PropTypes.number,
    created_at: PropTypes.instanceOf(Date),
    likes: PropTypes.number,
    comments: PropTypes.number,
    shares: PropTypes.number
  }),
  delayMultiplier: PropTypes.number,
  scrollTop: PropTypes.number,
  user_id: PropTypes.number,
  fixedVisibility: PropTypes.bool
};

export default Feed;
