/**
 *
 * Input
 *
 */

import React from "react";
import style from "./style.less";

class Input extends React.Component { // eslint-disable-line react/prefer-stateless-function
  static htmlValidTypes = [
    "color", "date", "datetime-local", "email", "month", "number", "range", "search", "tel", "time", "text", "url", "week", "password", "textarea"
  ];

  static validationRules = {
    email: /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/,
    tel: /^\+?\d{2}[- ]?\d{3}[- ]?\d{5}$/,
    number: /^\d+$/,
    color: /^#?[0-9A-F]{6}$/i,
    url: /^(https?:\/\/)?((([a-z\d]([a-z\d-]*[a-z\d])*)\.)+[a-z]{2,}|((\d{1,3}\.){3}\d{1,3}))(\:\d+)?(\/[-a-z\d%_.~+]*)*(\?[;&a-z\d%_.~+=-]*)?(\#[-a-z\d_]*)?$/i,
    password: /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/
  };

  static defaultProps = {
    value: "",
    placeholder: null,
    disabled: false,
    valid: true,
    validation: "",
    type: "",
    required: false,
    className: "",
    input: {
      id: null,
      name: null
    },
    onChange: () => {},
    onSubmit: false
  };

  validator = null;

  state = {
    value: "",
    valid: true
  };

  constructor(props) {
    super(props);
    this.state.value = (`${props.value}`).trim();
    const { required, valid, validation, type } = props;
    this.validator = validation === "" && type ? type : validation;
    if (this.validator) {
      const isFilled = required ? !!this.state.value : true;
      const rule = Input.validationRules[this.validator] || validation;
      this.validator = rule && new RegExp(rule);
      this.state.valid = valid && (!this.validator || (!required && !this.state.value) ? isFilled : this.validator.test(this.state.value));
    } else {
      this.state.valid = valid;
    }
  }

  isValid(value = this.state.value) {
    const { required } = this.props;
    const isFilled = required ? !!this.state.value : true;
    return (!this.validator || (!required && !value) ? isFilled : this.validator.test(value));
  }

  onSubmit(key) {
    const { valid, value } = this.state;
    const { required, onSubmit } = this.props;
    if (key === "Enter" && valid && typeof onSubmit === "function") {
      onSubmit(value, valid);
      this.setState({ valid: !required }); // value: '',
    }
  }

  onChange(value, callParent = true) {
    if (this.props.disabled) {
      return;
    }

    const isValid = this.isValid(value);
    this.setState({ value, valid: isValid });
    if (callParent) {
      this.props.onChange(value, isValid);
    }
  }

  componentWillReceiveProps(props) {
    const { validation, type, valid } = props;
    if (this.props.validation !== validation || this.props.type !== type) {
      this.validator = !validation && type ? type : validation;

      const rule = Input.validationRules[this.validator] || validation;
      this.validator = rule && new RegExp(rule);
    }

    if (this.state.value !== props.value) {
      this.onChange(props.value, false);
    }
  }

  render() {
    const { className, disabled, valid, type, placeholder, input } = this.props;
    const classes = [
      style.input,
      style[`input--${type}`],
      className,
      disabled && style["input--disabled"],
      !valid && style["input--invalid"]
    ].filter(Boolean).join(" ");

    return (
      <div className={classes}>
        <input
          value={this.state.value}
          type={Input.htmlValidTypes.includes(type) ? type : "text"}
          onChange={(e) => this.onChange(e.target.value)}
          onKeyPress={(e) => this.onSubmit(e.key)}
          disabled={disabled}
          readOnly={disabled}
          placeholder={placeholder}
          {...input}
        />
      </div>
    );
  }
}

export default Input;
