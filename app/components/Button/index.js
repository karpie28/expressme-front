/**
 *
 * Button
 *
 */

import React from "react";
import { Link } from "react-router-dom";
import style from "./style.less";

function Button({ href, children, className, center, styling, disabled, noMargin, props = {} }) {
  const isAbsolute = new RegExp("^(?:[a-z]+:)?//", "i");
  href = href || "#";
  styling = styling || "active";

  children = <span className={style.children}>{ children }</span>;

  let type = "a",
    element = null,
    classes = [
      style.button,
      center ? style["button--center"] : "",
      disabled ? style["button--disabled"] : "",
      noMargin ? style["button--no-margin"] : "",
      style[`button--style-${styling}`],
      className || ""
    ];

  if (typeof href === "function") {
    type = "function";
    classes.push(style[`button--${type}`]);
    element = (<a
      href="#"
      {...props}
      className={classes.filter(Boolean).join(" ")}
      onClick={(e) => { e.preventDefault(); e.stopPropagation(); href(e.target); }}
    >
      { children }
    </a>);
  } else if (href.startsWith("form.")) {
    type = "button";
    classes.push(style[`button--${type}`]);
    element = <button type={href.split(".")[1]} className={classes.filter(Boolean).join(" ")} {...props}>{ children }</button>;
  } else if (!isAbsolute.test(href) && href !== "#") {
    type = "link";
    classes.push(style[`button--${type}`]);
    element = <Link to={href} className={classes.filter(Boolean).join(" ")} {...props}>{ children }</Link>;
  } else {
    type = "a";
    classes.push(style[`button--${type}`]);
    element = <a href={href} className={classes.filter(Boolean).join(" ")} {...props}>{ children }</a>;
  }

  return element;
}

export default Button;
