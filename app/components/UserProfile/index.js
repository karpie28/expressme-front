/**
*
* UserProfile
*
*/

import React from "react";
import * as mouths from "images/mouths";
import style from "./style.less";
import { getUserRequest, postUserRequest } from "api/actions/UserActions";
import connect from "react-redux/es/connect/connect";
import { compose } from "redux";
import Dropzone from "react-dropzone";
const mouthsArray = Object.values(mouths);

class UserProfile extends React.Component {

  static defaultProps = {
    editable: true
  };

  state = {
    editName: false
  };

  oldContent = null;

  emitChange = (e) => {
     if (e.keyCode === 13) {
       e.preventDefault();
       e.target.innerHTML = e.target.innerText;
       this.setState({ editName: false }, () => this.emitBlur(e));
     }
  };

  emitBlur = () => {
    const { user, editable } = this.props;
    const content = this.content.innerText.trim();
    if (!editable || content === this.oldContent) {
      return;
    }

    const inputData = content.split(" ");
    const name = inputData.shift() || user.name;
    const surname = inputData.join(" ") || user.surname;
    this.props.postUser(name, surname);
    this.oldContent = content;
  };

  startEdit = () => {
    this.oldContent = this.content.innerText.trim();
    if (!this.state.editName && this.props.editable) {
      this.setState({ editName: true }, () => this.content.focus());
    }
  };

  onPhotoDrop = files => {
    const image = files[0];
    const { name, surname } = this.props.user;
    this.setState({ image, imageUrl: URL.createObjectURL(image) }, () => {
      this.props.postUser(name, surname, image);
    });
  };

  render() {
    const { user, editable } = this.props;
    const { editName, imageUrl } = this.state;

    const avatarClasses = [
      style.profile__avatar,
      editable && style["profile__avatar--editable"],
      user.isActive && style["profile__avatar--active"]
    ].filter(Boolean).join(" ");

    return (
      <div className={style.profile}>
        <Dropzone
          accept={["image/png", "image/jpeg"]}
          onDrop={this.onPhotoDrop}
          className={avatarClasses}
          style={{backgroundImage: `url('${imageUrl || user.avatar || "https://www.cockerandcarr.co.uk/wp-content/uploads/Person-placeholder.jpg"}')`}}
          multiple={false}
          maxSize={3000000}
          disabled={!editable}
        />
        <h3
          ref={ref => this.content = ref}
          contentEditable={editName}
          onKeyDown={this.emitChange}
          onBlur={this.emitBlur}
          onClick={this.startEdit}
        >{user.name} {user.surname}</h3>
        <div className={style.profile__mouth}>
          <img src={mouthsArray[user.last_emotion || 2]} />
        </div>
      </div>
    );
  }
}

UserProfile.propTypes = {

};

function mapDispatchToProps(dispatch) {
  return {
    postUser: (name, surname, avatar) => dispatch(postUserRequest({ name, surname, avatar }))
  };
}

const withConnect = connect(null, mapDispatchToProps);
export default compose(withConnect)(UserProfile);

