/**
*
* MainMenu
*
*/

import React from "react";
import Button from "components/Button";
import Modal from "react-responsive-modal";
import style from "./style.less";
// import styled from 'styled-components';


class MainMenu extends React.Component { // eslint-disable-line react/prefer-stateless-function
  state = {
    addNewFriends: false

  };

  render() {
    const { addNewFriends } = this.state;

    return (
      <div>
        <nav className={style.nav}>
          <Button href="/" styling="nav">My Wall</Button>
          <Button href="/friends" styling="nav">My Friends</Button>
          <Button href="/finder" styling="nav">Find New Friends</Button>
          <Button href="/logout" styling="nav">Logout</Button>
        </nav>
        <Modal open={addNewFriends} onClose={() => this.setState({ addNewFriends: false })} center>
          <h2>Simple centered modal</h2>
        </Modal>
      </div>
    );
  }
}

MainMenu.propTypes = {

};

export default MainMenu;
