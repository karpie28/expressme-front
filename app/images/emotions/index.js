import emotion1 from "./1.svg";
import emotion2 from "./2.svg";
import emotion3 from "./3.svg";
import emotion4 from "./4.svg";
import emotion5 from "./5.svg";

export {
  emotion1,
  emotion2,
  emotion3,
  emotion4,
  emotion5
};
