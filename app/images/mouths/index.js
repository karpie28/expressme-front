import mouth1 from "./1.svg";
import mouth2 from "./2.svg";
import mouth3 from "./3.svg";
import mouth4 from "./4.svg";
import mouth5 from "./5.svg";

export {
  mouth1,
  mouth2,
  mouth3,
  mouth4,
  mouth5
};
