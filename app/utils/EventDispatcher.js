export default class EventDispatcher  {

  events = {};

  on(event, callback) {
    let handlers = this.events[event] || [];
    handlers.push(callback);
    this.events[event] = handlers;
  }

  off(event, callback){
    let handlers = this.events[event] || [],
        index = handlers.indexOf(callback);
    if(index >= 0) {
      handlers.splice(index, 1);
    }
    this.events[event] = handlers;
  }

  clear(){
    this.events = {};
  }

  trigger(event, data = {}) {
    let handlers = this.events[event];

    if (!handlers || handlers.length < 1)
      return;

    [].forEach.call(handlers, handler => handler(data));
  }
}