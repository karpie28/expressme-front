import { takeEvery } from "redux-saga";
import { call, put, fork, take, select } from "redux-saga/effects";
import ApiService from "api/Routes";
import { getUser, postAutoLogin, postLogin, postSignup, postUser } from "api/sagas/UserSaga";
import { getFeeds, postFeed } from "api/sagas/FeedSaga";
import { GET_FEEDS_REQUEST, POST_FEED_REQUEST } from "api/actions/FeedActions";
import {
  AUTO_LOGIN_REQUEST,
  GET_USER_REQUEST,
  LOGIN_REQUEST,
  POST_USER_REQUEST,
  SIGNUP_REQUEST
} from "api/actions/UserActions";
import {
  DELETE_FRIEND_REQUEST,
  GET_FRIENDS_REQUEST,
  GET_INVITATIONS_REQUEST,
  SEARCH_FRIEND_REQUEST
} from "api/actions/FriendActions";
import { getInvitations, getFriends, deleteFriend, searchFriend } from "api/sagas/FriendSaga";

export const api = ApiService.create();

export default function* rootSaga() {
  yield [
    takeEvery(LOGIN_REQUEST, postLogin, api),
    takeEvery(AUTO_LOGIN_REQUEST, postAutoLogin, api),
    takeEvery(GET_USER_REQUEST, getUser, api),
    takeEvery(GET_FEEDS_REQUEST, getFeeds, api),
    takeEvery(POST_FEED_REQUEST, postFeed, api),
    takeEvery(SIGNUP_REQUEST, postSignup, api),
    takeEvery(POST_USER_REQUEST, postUser, api),
    takeEvery(GET_INVITATIONS_REQUEST, getInvitations, api),
    takeEvery(GET_FRIENDS_REQUEST, getFriends, api),
    takeEvery(DELETE_FRIEND_REQUEST, deleteFriend, api),
    takeEvery(SEARCH_FRIEND_REQUEST, searchFriend, api)
  ];
}
