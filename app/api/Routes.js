import apisauce from "apisauce";

const create = (baseURL = process.env.API_URL) => {
  const api = apisauce.create({
    baseURL,
    headers: {
      /* 'Cache-Control': 'no-cache',*/
      // 'Content-Type': 'application/json',
      // 'Accept': 'application/json'
    }
  });

  function buildForm(data) {
    const result = new FormData();

    for (const key in data) {
      if (typeof data[key] !== "undefined" && data.hasOwnProperty(key)) {
        result.append(key, data[key]);
      }
    }

    return result;
  }

  const setToken = (token) => api.setHeader("Authorization", `Bearer ${token}`);
  const removeToken = () => api.setHeader("Authorization", "");
  const getToken = () => api.headers.Authorization.replace("Bearer ", "") || "";

  const postLogin = (email, password) => api.post("/auth/login", { email, password });
  const postSignup = (email, password, name, surname, birth_date) => api.post("/auth/register", { email, password, name, surname, birth_date});
  const getLogout = () => api.get("/auth/logout");
  const getActivateToken = (token) => api.get(`/auth/activate/${token}`);

  const getMe = () => api.get("/me");
  const postMe = (name, surname, avatar) => api.post("/me", buildForm({ name, surname, avatar }));

  const getFeeds = (limit = 100, offset = 0) => api.get("/feeds", { limit, offset });
  const postFeed = (description, image) => api.post("/feeds", buildForm({ description, image }));
  const getLike = (feed_id) => api.get(`/feeds/${feed_id}/like`);
  const postLike = (feed_id) => api.post(`/feeds/${feed_id}/like`);
  const postComment = (feed_id, description) => api.post(`/feeds/${feed_id}/comment`, { description });

  const getInvitations = () => api.get("/friends/invitation");
  const getFriends = () => api.get("/friends");
  const deleteFriend = (friendId) => api.delete(`/friends/${friendId}`);
  const searchFriend = (search) => api.get("/friends/search", { search });

  const invite = (user_id_invited) => api.post("/friends/invitation", { user_id_invited });
  const acceptInvitation = (invitation_id) => api.put(`/friends/invitation/${invitation_id}/accept`);
  const rejectInvitation = (invitation_id) => api.put(`/friends/invitation/${invitation_id}/accept`);

  const getChat = (user_id, limit = 10, offset = 0) => api.get(`/chat/messages/${user_id}`, { limit, offset });
  const postChat = (user_id, message, image) => api.post(`/chat/messages/${user_id}`, buildForm({ message, image }));

  return {
    setToken,
    removeToken,
    getToken,

    postLogin,
    postSignup,
    getLogout,
    getActivateToken,

    getMe,
    postMe,

    getFeeds,
    postFeed,
    getLike,
    postLike,
    postComment,

    getInvitations,
    getFriends,
    deleteFriend,
    searchFriend,

    invite,
    acceptInvitation,
    rejectInvitation,

    getChat,
    postChat,
  };
};

// let's return back our create method as the default.
export default {
  create
};

