import { createAction } from "redux-actions";

export const GET_FEEDS_REQUEST = "GET_FEEDS_REQUEST";
export const GET_FEEDS_SUCCESS = "GET_FEEDS_SUCCESS";
export const GET_FEEDS_FAILURE = "GET_FEEDS_FAILURE";
export const getFeedsRequest = createAction(GET_FEEDS_REQUEST);
export const getFeedsSuccess = createAction(GET_FEEDS_SUCCESS);
export const getFeedsFailure = createAction(GET_FEEDS_FAILURE);

export const POST_FEED_REQUEST = "POST_FEED_REQUEST";
export const POST_FEED_SUCCESS = "POST_FEED_SUCCESS";
export const POST_FEED_FAILURE = "POST_FEED_FAILURE";
export const postFeedRequest = createAction(POST_FEED_REQUEST);
export const postFeedSuccess = createAction(POST_FEED_SUCCESS);
export const postFeedFailure = createAction(POST_FEED_FAILURE);

