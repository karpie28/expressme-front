import { createAction } from "redux-actions";

export const LOGIN_REQUEST = "LOGIN_REQUEST";
export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const LOGIN_FAILURE = "LOGIN_FAILURE";
export const loginRequest = createAction(LOGIN_REQUEST);
export const loginSuccess = createAction(LOGIN_SUCCESS);
export const loginFailure = createAction(LOGIN_FAILURE);

export const AUTO_LOGIN_REQUEST = "AUTO_LOGIN_REQUEST";
export const AUTO_LOGIN_SUCCESS = "AUTO_LOGIN_SUCCESS";
export const AUTO_LOGIN_FAILURE = "AUTO_LOGIN_FAILURE";
export const autoLoginRequest = createAction(AUTO_LOGIN_REQUEST);
export const autoLoginSuccess = createAction(AUTO_LOGIN_SUCCESS);
export const autoLoginFailure = createAction(AUTO_LOGIN_FAILURE);

export const SIGNUP_REQUEST = "SIGNUP_REQUEST";
export const SIGNUP_SUCCESS = "SIGNUP_SUCCESS";
export const SIGNUP_FAILURE = "SIGNUP_FAILURE";
export const signupRequest = createAction(SIGNUP_REQUEST);
export const signupSuccess = createAction(SIGNUP_SUCCESS);
export const signupFailure = createAction(SIGNUP_FAILURE);

export const GET_USER_REQUEST = "GET_USER_REQUEST";
export const GET_USER_SUCCESS = "GET_USER_SUCCESS";
export const GET_USER_FAILURE = "GET_USER_FAILURE";
export const getUserRequest = createAction(GET_USER_REQUEST);
export const getUserSuccess = createAction(GET_USER_SUCCESS);
export const getUserFailure = createAction(GET_USER_FAILURE);

export const POST_USER_REQUEST = "POST_USER_REQUEST";
export const POST_USER_SUCCESS = "POST_USER_SUCCESS";
export const POST_USER_FAILURE = "POST_USER_FAILURE";
export const postUserRequest = createAction(POST_USER_REQUEST);
export const postUserSuccess = createAction(POST_USER_SUCCESS);
export const postUserFailure = createAction(POST_USER_FAILURE);
