import { createAction } from "redux-actions";

export const GET_INVITATIONS_REQUEST = "GET_INVITATIONS_REQUEST";
export const GET_INVITATIONS_SUCCESS = "GET_INVITATIONS_SUCCESS";
export const GET_INVITATIONS_FAILURE = "GET_INVITATIONS_FAILURE";
export const getInvitationsRequest = createAction(GET_INVITATIONS_REQUEST);
export const getInvitationsSuccess = createAction(GET_INVITATIONS_SUCCESS);
export const getInvitationsFailure = createAction(GET_INVITATIONS_FAILURE);


export const GET_FRIENDS_REQUEST = "GET_FRIENDS_REQUEST";
export const GET_FRIENDS_SUCCESS = "GET_FRIENDS_SUCCESS";
export const GET_FRIENDS_FAILURE = "GET_FRIENDS_FAILURE";
export const getFriendsRequest = createAction(GET_FRIENDS_REQUEST);
export const getFriendsSuccess = createAction(GET_FRIENDS_SUCCESS);
export const getFriendsFailure = createAction(GET_FRIENDS_FAILURE);

export const DELETE_FRIEND_REQUEST = "DELETE_FRIEND_REQUEST";
export const DELETE_FRIEND_SUCCESS = "DELETE_FRIEND_SUCCESS";
export const DELETE_FRIEND_FAILURE = "DELETE_FRIEND_FAILURE";
export const deleteFriendRequest = createAction(DELETE_FRIEND_REQUEST);
export const deleteFriendSuccess = createAction(DELETE_FRIEND_SUCCESS);
export const deleteFriendFailure = createAction(DELETE_FRIEND_FAILURE);

export const SEARCH_FRIEND_REQUEST = "SEARCH_FRIEND_REQUEST";
export const SEARCH_FRIEND_SUCCESS = "SEARCH_FRIEND_SUCCESS";
export const SEARCH_FRIEND_FAILURE = "SEARCH_FRIEND_FAILURE";
export const searchFriendRequest = createAction(SEARCH_FRIEND_REQUEST);
export const searchFriendSuccess = createAction(SEARCH_FRIEND_SUCCESS);
export const searchFriendFailure = createAction(SEARCH_FRIEND_FAILURE);
