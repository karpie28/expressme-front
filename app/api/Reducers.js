import { combineReducers } from "redux-immutable";
import Immutable from "immutable";

import {
  LOGIN_SUCCESS,
  LOGIN_FAILURE,
  LOGIN_REQUEST,
  GET_USER_REQUEST,
  GET_USER_FAILURE,
  GET_USER_SUCCESS,
  AUTO_LOGIN_REQUEST,
  AUTO_LOGIN_SUCCESS,
  AUTO_LOGIN_FAILURE,
  POST_USER_FAILURE, POST_USER_SUCCESS, POST_USER_REQUEST
} from "./actions/UserActions";

import {
  GET_FEEDS_SUCCESS, GET_FEEDS_FAILURE, GET_FEEDS_REQUEST,
  POST_FEED_SUCCESS, POST_FEED_FAILURE, POST_FEED_REQUEST
} from "./actions/FeedActions";
import {
  DELETE_FRIEND_FAILURE, DELETE_FRIEND_REQUEST, DELETE_FRIEND_SUCCESS,
  GET_FRIENDS_FAILURE, GET_FRIENDS_REQUEST, GET_FRIENDS_SUCCESS,
  GET_INVITATIONS_FAILURE,
  GET_INVITATIONS_REQUEST,
  GET_INVITATIONS_SUCCESS, SEARCH_FRIEND_FAILURE, SEARCH_FRIEND_REQUEST, SEARCH_FRIEND_SUCCESS
} from "api/actions/FriendActions";

const initial = Immutable.Record({
  user: {
    fetching: false,
    error: null,
    data: {}
  },
  feed: {
    fetching: false,
    error: null,
    list: {}
  },
  friend: {
    fetching: false,
    error: null,
    invitations: [],
    list: [],
    search: []
  }
});

function user(state = initial.user, { type, payload }) {
  switch (type) {
    case LOGIN_REQUEST:
    case GET_USER_REQUEST:
    case AUTO_LOGIN_REQUEST:
      return { ...state, fetching: true, error: null, data: {} };
    case LOGIN_FAILURE:
    case POST_USER_FAILURE:
      return { ...state, fetching: false, error: payload.error };
    case LOGIN_SUCCESS:
    case GET_USER_SUCCESS:
    case AUTO_LOGIN_SUCCESS:
    case POST_USER_SUCCESS:
      return { ...state, fetching: false, error: null, data: payload.data };
    case GET_USER_FAILURE:
      return { ...state, fetching: false, error: payload.error, data: {} };
    case POST_USER_REQUEST:
      return { ...state, fetching: false, error: null };
    case AUTO_LOGIN_FAILURE:
      return { ...state, fetching: false, error: null, data: {} };
  }
  return state;
}

function feed(state = initial.feed, { type, payload }) {
  switch (type) {
    case GET_FEEDS_REQUEST:
      return { ...state, fetching: true, error: null, list: {} };
    case GET_FEEDS_FAILURE:
    case POST_FEED_FAILURE:
      return { ...state, fetching: false, error: payload.error };
    case GET_FEEDS_SUCCESS:
      return { ...state, fetching: false, error: null, list: payload.data };
    case POST_FEED_REQUEST:
      return { ...state, fetching: true, error: null, data: {} };
    case POST_FEED_SUCCESS:
      return { ...state, fetching: false, error: null, data: payload.data };
  }
  return state;
}

function friend(state = initial.friend, { type, payload }) {
  switch (type) {
    case GET_INVITATIONS_REQUEST:
      return { ...state, fetching: true, error: null, invitations: [] };
    case GET_INVITATIONS_FAILURE:
    case DELETE_FRIEND_FAILURE:
    case GET_FRIENDS_FAILURE:
    case SEARCH_FRIEND_FAILURE:
      return { ...state, fetching: false, error: payload.error };
    case GET_INVITATIONS_SUCCESS:
      return { ...state, fetching: false, error: null, invitations: payload.data };
    case GET_FRIENDS_REQUEST:
      return { ...state, fetching: true, error: null, list: [] };
    case GET_FRIENDS_SUCCESS:
      return { ...state, fetching: false, error: null, list: payload.data };
    case DELETE_FRIEND_SUCCESS:
      return { ...state, fetching: false, error: null };
    case DELETE_FRIEND_REQUEST:
      return { ...state, fetching: true, error: null };
    case SEARCH_FRIEND_REQUEST:
      return { ...state, fetching: true, error: null, search: [] };
    case SEARCH_FRIEND_SUCCESS:
      return { ...state, fetching: false, error: null, search: payload.data };
  }
  return state;
}

export default combineReducers(
  { user, feed, friend }, initial
);
