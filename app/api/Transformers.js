export function transformMessage(item) {
  // We need to replace space between date and time with T
  // to make string format compatible with ISO 8601.
  // Otherwise, Safari and IE will throw exception here.

  if (typeof item.created_at === "string") {
    item.created_at = new Date(item.created_at.replace(" ", "T"));
    item.created_at.setHours(item.created_at.getHours() + 1);
  }

  if (typeof item.updated_at === "string") {
    item.updated_at = new Date(item.updated_at.replace(" ", "T"));
    item.updated_at.setHours(item.updated_at.getHours() + 1);
  }

  return item;
}


export function transformMessages(list, keyName = "id") {
  return Object.values(list).reduce((result, item) => {
    // create array element with item.id as key
    result[item[keyName]] = transformMessage(item);
    return result;
  }, {});
}

/**
 * Retrieves input data from a form and returns it as a JSON object.
 * @param  {HTMLFormControlsCollection} elements  the form elements
 * @return {Object}                               form data as an object literal
 */
export function formToJSON(elements) {
  return [].reduce.call(elements, (data, element) => {
    if (element.name) {
      data[element.name] = element.value;
    }
    return data;
  }, {});
}
