import { call, put, fork, take, select } from "redux-saga/effects";
import * as actions from "../actions/UserActions";
import { transformMessages } from "../Transformers";

export function* postLogin(api, { payload }) {
  const { email, password } = payload;
  const response = yield call(api.postLogin, email, password);
  if (response.ok) {
    localStorage.token = response.data.access_token;
    api.setToken(localStorage.token);
    yield put(actions.loginSuccess({ data: response.data }));
    yield call(getUser, api);
  } else {
    alert(response.data.error || response.problem);
    yield put(actions.loginFailure({ error: response.data || response.problem }));
  }
}

export function* postAutoLogin(api) {
  const response = yield call(api.getMe);
  if (response.ok) {
    yield put(actions.autoLoginSuccess({ data: response.data }));
    yield call(getUser, api);
  } else {
    yield put(actions.autoLoginFailure({ error: response.data || response.problem }));
  }
}

export function* postSignup(api, { payload }) {
  const { email, password, name, surname, birthDate } = payload;
  const response = yield call(api.postSignup, email, password, name, surname, `${birthDate} 00:00:00`);
  if (response.ok) {
    localStorage.token = response.data.access_token;
    api.setToken(localStorage.token);
    yield put(actions.signupSuccess({ data: response.data }));
  } else {
    yield put(actions.signupFailure({ error: response.data || response.problem }));
  }
}

export function* getUser(api) {
  const response = yield call(api.getMe);
  if (response.ok) {
    yield put(actions.getUserSuccess({ data: response.data }));
  } else {
    yield put(actions.getUserFailure({ error: response.data || response.problem }));
  }
}

export function* postUser(api, { payload }) {
  const { name, surname, avatar } = payload;
  const response = yield call(api.postMe, name, surname, avatar);
  if (response.ok) {
    yield put(actions.postUserSuccess({ data: response.data }));
  } else {
    yield put(actions.postUserRequest({ error: response.data || response.problem }));
  }
}
