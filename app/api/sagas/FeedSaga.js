import { call, put, fork, take, select } from "redux-saga/effects";
import * as actions from "api/actions/FeedActions";
import { transformMessages, transformMessage } from "api/Transformers";

export function* getFeeds(api, { payload }) {
  const { limit, offset } = payload || {};
  const response = yield call(api.getFeeds, limit, offset);


  if (response.ok) {
    const data = response.data.map(item => {
      if (item.comments && item.comments.length > 0) {
        item.comments = item.comments.map(transformMessage);
      }
      return item;
    });
    yield put(actions.getFeedsSuccess({ data: transformMessages(data, "feed_id") }));
  } else {
    yield put(actions.getFeedsFailure({ error: response.data || response.problem }));
  }
}

export function* postFeed(api, { payload }) {
  const { description, image } = payload;
  const response = yield call(api.postFeed, description, image);
  if (response.ok) {
    yield put(actions.postFeedSuccess({ data: transformMessage(response.data) }));
  } else {
    yield put(actions.postFeedFailure({ error: response.data || response.problem }));
  }
}
