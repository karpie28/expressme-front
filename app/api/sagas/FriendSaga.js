import { call, put, fork, take, select } from "redux-saga/effects";
import * as actions from "api/actions/FriendActions";
import { transformMessages, transformMessage } from "api/Transformers";

export function* getInvitations(api) {
  const response = yield call(api.getInvitations);
  if (response.ok) {
    yield put(actions.getInvitationsSuccess({ data: response.data }));
  } else {
    yield put(actions.getInvitationsFailure({ error: response.data || response.problem }));
  }
}

export function* getFriends(api) {
  const response = yield call(api.getFriends);
  if (response.ok) {
    yield put(actions.getFriendsSuccess({ data: response.data }));
  } else {
    yield put(actions.getFriendsFailure({ error: response.data || response.problem }));
  }
}

export function* deleteFriend(api, { payload }) {
  const { friendId } = payload;
  const response = yield call(api.deleteFriend, friendId);
  if (response.ok) {
    yield put(actions.deleteFriendSuccess({ data: response.data }));
  } else {
    yield put(actions.deleteFriendFailure({ error: response.data || response.problem }));
  }
}

export function* searchFriend(api, { payload }) {
  const { search } = payload;
  const response = yield call(api.searchFriend, search);
  if (response.ok) {
    yield put(actions.searchFriendSuccess({ data: response.data }));
  } else {
    yield put(actions.searchFriendFailure({ error: response.data || response.problem }));
  }
}
