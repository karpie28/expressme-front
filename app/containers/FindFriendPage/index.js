/**
 *
 * FeedPage
 *
 */

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { compose } from "redux";
import style from "./style.less";
import { deleteFriendRequest, getFriendsRequest, searchFriendRequest } from "api/actions/FriendActions";
import FriendBox from "components/FriendBox";
import Input from "components/Input";
import Button from "components/Button";
import { Link } from "react-router-dom";

export class FindFriendPage extends React.Component { // eslint-disable-line react/prefer-stateless-function

  state = {
    feeds: [],
    feed: "",
    placeholder: 0,
    search: "",
    requestSent: false
  };

  onSearch = (e) => {
    e.preventDefault();
    const { search } = this.state;

    if (!search) {
      return;
    }
    this.setState({ requestSent: true });
    this.props.searchFriend(search);
  };

  constructor(props) {
    super(props);
  }

  componentWillMount() {
   // this.props.getFriends();
  }

  render() {
    const { friend, addFriend } = this.props;
    const { search, requestSent } = this.state;

    return (
      <div className={style.search}>
        <form className={style.search__box} onSubmit={this.onSearch}>
          <Input className={style.search__input} placeholder="Search friend..." value={search} onChange={search => this.setState({ search })} />
          <Button noMargin center styling="yellow" href="form.submit">Search</Button>
        </form>
        <div className={style.profiles}>
          { friend.search.map((item, index) => <FriendBox {...item} addFriend={true} key={index} delay={index} />) }
        </div>
        {
          search && requestSent && !friend.fetching && friend.search.length === 0 && (
            <div className={style.empty}>
              <h3>Can not find any friends.</h3>
              <span>Change your query and try again.</span>
            </div>
          )
        }
      </div>
    );
  }
}

FindFriendPage.propTypes = {
  getFeeds: PropTypes.func.isRequired,
  scrollTop: PropTypes.number
};


function mapDispatchToProps(dispatch) {
  return {
    searchFriend: search => dispatch(searchFriendRequest({ search }))
  };
}

function mapStateToProps(mapState) {
  const state = mapState.toJS();
  const { friend } = state.api;
  return {
    friend
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export default compose(
  withConnect,
)(FindFriendPage);
