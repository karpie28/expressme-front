/**
 *
 * LoginPage
 *
 */

import React from "react";
import { Helmet } from "react-helmet";
import { api } from "api/Sagas";
import Button from "components/Button";
import * as mouths from "images/mouths";
import style from "./style.less";

export default class ActivatePage extends React.Component { // eslint-disable-line react/prefer-stateless-function
  state = {
    error: false,
    success: false
  };

  componentWillMount() {
    const { token } = this.props.match.params;
    api.getActivateToken(token).then(response => this.setState({ error: response.data && response.data.message, success: response.ok }));
  }

  render() {
    const { error, success } = this.state;

    const errorClasses = [
      style.login__error,
      (error || success) && style["login__error--visible"]
    ].filter(Boolean).join(" ");

    return (
      <div className={style.login}>
        <Helmet>
          <title>Login - ExpressMe</title>
          <meta name="description" content="Description of LoginPage" />
        </Helmet>
        <div className={style.login__header}>
          <h1>ExpressMe</h1>
          <h3>Social media that can feel!</h3>
        </div>
        <div className={style.login__mouths}>
          { Object.values(mouths).reverse().map((mouth, key) => <img key={key} src={mouth} />) }
        </div>
        <span className={errorClasses}>
          {
            error ? (error || "Invalid token") : <span>Account has been successfully activated.<br />You can log in now!</span>
          }
        </span>
        <span><Button href="/" className={style.button} noMargin>Login</Button></span>
      </div>
    );
  }
}
