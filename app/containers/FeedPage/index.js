/**
 *
 * FeedPage
 *
 */

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { compose } from "redux";
import { Link } from 'react-router-dom'
import Dropzone from "react-dropzone";
import TextAreaAutoresize from "react-autosize-textarea";
import { getFeedsRequest, postFeedRequest } from "api/actions/FeedActions";
import Feed from "components/Feed";
import Button from "components/Button";
import { photo, upload } from "images/icons";
import style from "./style.less";

export class FeedPage extends React.Component { // eslint-disable-line react/prefer-stateless-function

  static contextTypes = {
    rwd: React.PropTypes.object
  };

  static placeholders = [
    "What's on your mind?",
    "What's up?",
    "How are you today?",
    "How are you doing?",
    "How was your day?"
  ];

  state = {
    feeds: [],
    feed: "",
    placeholder: 0
  };

  constructor(props) {
    super(props);
    this.state.placeholder = Math.floor(Math.random() * FeedPage.placeholders.length);
  }

  componentWillMount() {
    //this.prepareMediaStream();
    this.props.getFeeds();
  }

  componentWillReceiveProps(props) {
    const { feed } = props;
    const { fetching, list, data: item } = feed;
    const state = {};
    if (!fetching) {
      if (list !== this.props.feed.list) {
        state.feeds = Object.values(list).reverse();
      }
      if (item && item !== this.props.feed.data && typeof this.state.feeds[item.feed_id] === "undefined") {
        state.feeds = (state.feeds || this.state.feeds || []);
        state.feeds.unshift(item);
      }
    }

    this.setState(state);
  }

  onChange(value) {
    this.setState({ feed: value });
  }

  onSubmit = () => {
    const { feed, image } = this.state;
    if (feed.length === 0) {
      alert("Enter your feed description");
      return;
    }
    this.props.postFeed(feed, image);
    this.setState({ feed: "", image: null });
  };

  onPhotoDrop = files => this.setState({ image: files[0], imageUrl: URL.createObjectURL(files[0]) });

  onAddImage = () => this.uploadRef && this.uploadRef.open();

  onMakePhoto = async () => {
    if (!this.imageCapture) {
      return alert("Your browser doesn't support capturing images");
    }
    const image = await this.imageCapture.takePhoto();
    this.setState({ image });
  };

  async prepareMediaStream() {
    try {
      const mediaStream = await window.navigator.mediaDevices.getUserMedia({ video: true })
      this.stream = mediaStream;
      const mediaStreamTrack = mediaStream.getVideoTracks()[0];
      this.imageCapture = new window.ImageCapture(mediaStreamTrack);
    } catch (ex) {
      console.log(ex);
    }
  }

  render() {
    const { feeds, feed, placeholder, image, imageUrl } = this.state;
    const { scrollTop, user } = this.props;

    const photoUploadButtons = (
      <div>
        { this.imageCapture && <Button className={style.icon} noMargin href={this.onMakePhoto}>{ photo }</Button> }
        <Button noMargin className={style.icon} href={this.onAddImage}>{ upload }</Button>
      </div>
    );
    return (
      <div>
        <Dropzone
          accept={["image/png", "image/jpeg"]}
          onDrop={this.onPhotoDrop}
          className={style.newFeed}
          multiple={false}
          ref={ref => this.uploadRef = ref}
          maxSize={3000000}
          disableClick
        >
          <TextAreaAutoresize
            rows={1}
            className={style.newFeed__input}
            value={feed}
            onChange={(e) => this.onChange(e.target.value)}
            placeholder={FeedPage.placeholders[placeholder]}
          >
            { feed }
          </TextAreaAutoresize>
          <div className={style.newFeed__buttons}>
            {
              !image
                ? photoUploadButtons
                : (
                  <div
                    onClick={() => this.setState({ image: null })}
                    className={style.preview}
                    style={{backgroundImage: `url(${imageUrl})`}}
                  />
                )
            }
            <Button noMargin center styling="yellow" href={this.onSubmit}>Submit</Button>
          </div>
        </Dropzone>
        {
          feeds.map((item, index) => (
            <Feed
              feed={item}
              key={index}
              delayMultiplier={index}
              scrollTop={scrollTop}
              user_id={user.user_id}
              user_name={`${user.name} ${user.surname}`}
              fixedVisibility={this.context.rwd.screen <= 768}
            />
          ))
        }
        { !feeds.fetching && feeds.length === 0 && (
          <div className={style.empty}>
            <h3>Your wall is so empty! 😲</h3>
            <span>Write your first post or <Link to="/finder">Get some friends</Link>.</span>
          </div>
        ) }
      </div>
    );
  }
}

FeedPage.propTypes = {
  getFeeds: PropTypes.func.isRequired,
  scrollTop: PropTypes.number
};


function mapDispatchToProps(dispatch) {
  return {
    getFeeds: (limit, offset) => dispatch(getFeedsRequest({ limit, offset })),
    postFeed: (description, image) => dispatch(postFeedRequest({ description, image }))
  };
}

function mapStateToProps(mapState) {
  const state = mapState.toJS();
  const { feed, user } = state.api;
  return {
    feed,
    user: user.data || {}
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export default compose(
  withConnect,
)(FeedPage);
