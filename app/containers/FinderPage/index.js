/**
 *
 * FeedPage
 *
 */

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { compose } from "redux";
import style from "./style.less";
import { getFriendsRequest } from "api/actions/FriendActions";

export class FinderPage extends React.Component { // eslint-disable-line react/prefer-stateless-function

  state = {
    feeds: [],
    feed: "",
    placeholder: 0
  };

  constructor(props) {
    super(props);
  }

  componentWillMount() {
    this.props.getFriends();
  }

  render() {
    const { friend } = this.props;

    return (
      <div>
        { friend.list.map(x => console.log(x)) }
      </div>
    );
  }
}

FinderPage.propTypes = {
  getFeeds: PropTypes.func.isRequired,
  scrollTop: PropTypes.number
};


function mapDispatchToProps(dispatch) {
  return {
    getFriends: () => dispatch(getFriendsRequest())
  };
}

function mapStateToProps(mapState) {
  const state = mapState.toJS();
  const { friend } = state.api;
  return {
    friend
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export default compose(
  withConnect,
)(FinderPage);
