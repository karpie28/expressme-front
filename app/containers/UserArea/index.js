/**
 *
 * UserArea
 *
 */

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { compose } from "redux";
import { Switch, Route } from "react-router-dom";
import FeedPage from "containers/FeedPage";
import UserProfile from "components/UserProfile";
import style from "./style.less";
import ActivityList from "components/ActivityList";
import MainMenu from "components/MainMenu";
import NotificationSection from "components/NotificationSection";
import FriendsPage from "containers/FriendsPage";
import FinderPage from "containers/FinderPage";
import FindFriendPage from "containers/FindFriendPage";
import Chat from "components/Chat";

export class UserArea extends React.Component { // eslint-disable-line react/prefer-stateless-function
  static contextTypes = {
    rwd: React.PropTypes.object
  };

  isComponentMounted = false;
  leftSidebar = null;

  state = {
    scrollTop: 0,
    leftSidebarWidth: 230
  };

  onScroll = (event) => {
    const { scrollTop: oldScrollTop } = this.state;
    const scrollTop = window.pageYOffset || document.documentElement.scrollTop;
    if (Math.abs(oldScrollTop - scrollTop) > 100) {
      this.setState({ scrollTop });
    }
  };

  onResize = () => {
    if (!this.leftSidebar || !this.rightSidebar || this.context.rwd.screen <= 768) {
      return;
    }
    this.setState({ leftSidebarWidth: this.leftSidebar.offsetWidth, rightSidebarWidth: this.rightSidebar.offsetWidth });
  };

  animationLoop = () => {
    if (!this.isComponentMounted || this.context.rwd.screen <= 768) {
      return;
    }
    const { scrollTop: oldScrollTop } = this.state;
    const scrollTop = window.pageYOffset || document.documentElement.scrollTop;
    if (Math.abs(oldScrollTop - scrollTop) > 100) {
      this.setState({ scrollTop }, () => requestAnimationFrame(this.animationLoop));
    } else {
      requestAnimationFrame(this.animationLoop);
    }
  };

  componentDidMount() {
    this.isComponentMounted = true;
    this.onResize();
    requestAnimationFrame(this.animationLoop);
    //window.addEventListener("scroll", this.onScroll);
    window.addEventListener("resize", this.onResize);
  }

  componentWillUnmount() {
    this.isComponentMounted = false;
    //window.removeEventListener("scroll", this.onScroll);
    window.removeEventListener("resize", this.onResize);
  }

  render() {
    const { user, logout } = this.props;
    const { leftSidebarWidth, rightSidebarWidth, scrollTop } = this.state;

    const leftSidebarClasses = [
      style.sidebar,
      style.sidebar__left
    ].filter(Boolean).join(" ");

    const rightSidebarClasses = [
      style.sidebar,
      style.sidebar__right
    ].filter(Boolean).join(" ");

    return (
      <div>
        <Helmet>
          <title>ExpressMe.pl</title>
        </Helmet>
        <div className={style.container}>
          <aside className={leftSidebarClasses} ref={ref => this.leftSidebar = ref}>
            <div className={style.fixed} style={{ width: `${leftSidebarWidth}px` }}>
              <UserProfile user={user.data} />
              <NotificationSection />
              <MainMenu />
            </div>
          </aside>
          <main className={style.content}>
            <Switch>
              <Route path="/" exact render={props => <FeedPage {...props} scrollTop={scrollTop} /> } />
              <Route path="/logout" component={logout} />
              <Route path="/friends" component={FriendsPage} />
              <Route path="/finder" component={FindFriendPage} />
            </Switch>
          </main>
          <aside className={rightSidebarClasses} ref={ref => this.rightSidebar = ref}>
            <div className={style.fixed} style={{ width: `${rightSidebarWidth}px` }}>
              <Chat />
            </div>
          </aside>
        </div>
      </div>
    );
  }
}

UserArea.propTypes = {
  dispatch: PropTypes.func.isRequired,
  logout: PropTypes.func
};


function mapDispatchToProps(dispatch) {
  return {
    dispatch
  };
}

function mapStateToProps(mapState) {
  const state = mapState.toJS();
  const { user } = state.api;
  return {
    user
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export default compose(
  withConnect,
)(UserArea);
