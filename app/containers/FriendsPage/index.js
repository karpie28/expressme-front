/**
 *
 * FeedPage
 *
 */

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { compose } from "redux";
import style from "./style.less";
import { deleteFriendRequest, getFriendsRequest } from "api/actions/FriendActions";
import FriendBox from "components/FriendBox";
import { Link } from "react-router-dom";

export class FriendsPage extends React.Component { // eslint-disable-line react/prefer-stateless-function

  state = {
    feeds: [],
    feed: "",
    placeholder: 0
  };

  constructor(props) {
    super(props);
  }

  componentWillMount() {
    this.props.getFriends();
  }

  render() {
    const { friend, deleteFriend } = this.props;

    return (
      <div className={style.profiles}>
        { friend.list.map((item, index) => <FriendBox {...item} deleteFriend={deleteFriend} key={index} delay={index} />) }
        { friend.list.length == 0 && <div className={style.empty}>
          <h3>Your friend list is so empty too! 😲</h3>
          <span>Write your first post or <Link to="/finder">Get some friends</Link>.</span>
        </div>}

      </div>
    );
  }
}

FriendsPage.propTypes = {
  getFeeds: PropTypes.func.isRequired,
  scrollTop: PropTypes.number
};


function mapDispatchToProps(dispatch) {
  return {
    getFriends: () => dispatch(getFriendsRequest()),
    deleteFriend: (friendId) => dispatch(deleteFriendRequest({ friendId }))
  };
}

function mapStateToProps(mapState) {
  const state = mapState.toJS();
  const { friend } = state.api;
  return {
    friend
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export default compose(
  withConnect,
)(FriendsPage);
