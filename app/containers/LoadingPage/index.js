/**
 *
 * LoadingPage
 *
 */

import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { compose } from "redux";
import * as mouths from "images/mouths";
import style from "./style.less";

const mouthsArray = Object.values(mouths);

class LoadingPage extends Component {
  static defaultProps = {
    isVisible: false
  };

  timeout = null;

  state = {
    isVisible: false,
    currentMouth: Math.floor(Math.random() * mouthsArray.length)
  };

  constructor(props) {
    super(props);
    this.componentWillReceiveProps(props);
  }

  componentWillReceiveProps(props) {
    const { isVisible } = props;
    if (isVisible !== this.props.isVisible) {
      //clearTimeout(this.timeout);
      this.timeout = setTimeout(() => this.setState({ isVisible }), isVisible ? 500 : 1000);
    }
  }

  componentWillUnmount() {
    clearTimeout(this.timeout);
  }

  render() {
    const { isVisible, currentMouth } = this.state;
    const randomMouth = mouthsArray[currentMouth];

    const classes = [
      style.loading,
      isVisible && style["loading--visible"]
    ].filter(Boolean).join(" ");

    return (
      <div className={classes}>
        <div className={style.loading__box}>
          <img src={randomMouth} alt="Loading..." />
        </div>
      </div>
    );
  }
}

LoadingPage.propTypes = {
  dispatch: PropTypes.func.isRequired
};


function mapDispatchToProps(dispatch) {
  return {
    dispatch
  };
}

const withConnect = connect(null, mapDispatchToProps);

export default compose(
  withConnect,
)(LoadingPage);
