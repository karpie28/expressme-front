/**
 *
 * LoginPage
 *
 */

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { compose } from "redux";
import Input from "components/Input";
import Button from "components/Button";
import * as mouths from "images/mouths";
import style from "./style.less";
import { loginRequest, signupRequest } from "api/actions/UserActions";
import { api } from "api/Sagas";

export class SignupPage extends React.Component { // eslint-disable-line react/prefer-stateless-function
  state = {
    password: "",
    email: "",
    name: "",
    surname: "",
    birthDate: "",
    error: false,
    success: false
  };

  handleSubmit = (e) => {
    e.preventDefault();
    const { email, password, name, surname, birthDate } = this.state;
    api.postSignup(email, password, name, surname, `${birthDate} 00:00:00`)
      .then(response => {
        if (!response.ok) {
          alert(response.data.error);
        }
        this.setState({ error: (response.data && response.data.error), success: response.ok })
      });
  };

  componentWillReceiveProps(props){
    if (props.error !== this.props.error) {
      this.setState({ error: props.error });
    }
  }

  render() {
    const { error, email, success, password, name, surname, birthDate } = this.state;

    const errorClasses = [
      style.login__error,
      error || success && style["login__error--visible"]
    ].filter(Boolean).join(" ");

    return (
      <div className={style.login}>
        <Helmet>
          <title>Sign up - ExpressMe</title>
        </Helmet>
        <div className={style.login__header}>
          <h1>ExpressMe</h1>
          <h3>Social media that can feel!</h3>
        </div>
        <div className={style.login__mouths}>
          { Object.values(mouths).reverse().map((mouth, key) => <img key={key} src={mouth} />) }
        </div>
        { success ? (
          <span className={errorClasses}>
            You created an account!<br />
            Please check your email and activate your account.
          </span>
        ) : (
          <span className={errorClasses}>
            An error occurred!<br />
            { error || "Please make sure you have entered the correct data and try again." }
          </span>
        ) }
        { !success &&
          <form className={style.login__form} onSubmit={this.handleSubmit}>
            <div>
              <Input
                className={style.input}
                type="email"
                placeholder="Email"
                value={email}
                onChange={email => this.setState({ email })}
                required
                input={{ required: true }}
              />
              <Input
                className={style.input}
                type="password"
                placeholder="Password"
                value={password}
                onChange={password => this.setState({ password })}
                required
                input={{ required: true }}
              />
            </div>
            <div>
              <Input
                className={style.input}
                placeholder="Name"
                value={name}
                onChange={name => this.setState({ name })}
                required
                input={{ required: true }}
              />
              <Input
                className={style.input}
                placeholder="Surname"
                value={surname}
                onChange={surname => this.setState({ surname })}
                required
                input={{ required: true }}
              />
            </div>
            <div>
              <Input
                className={style.input}
                type="date"
                placeholder="Birth Date"
                value={birthDate}
                onChange={birthDate => this.setState({ birthDate })}
                required
                input={{ required: true }}
              />
            </div>
            <Button className={style.button} href="form.submit">Create account</Button>
          </form>
        }
        { success && <span><Button href="/" className={style.button} noMargin>Login</Button></span> }
      </div>
    );
  }
}

SignupPage.propTypes = {
  signup: PropTypes.func.isRequired,
  error: PropTypes.object,
  success: PropTypes.bool
};


function mapDispatchToProps(dispatch) {
  return {
    signup: (email, password, name, surname, birthDate) => dispatch(signupRequest({ email, password, name, surname, birthDate }))
  };
}

const withConnect = connect(null, mapDispatchToProps);

export default compose(
  withConnect,
)(SignupPage);
