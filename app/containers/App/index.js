/**
 *
 * App.js
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from "react";
import { Route, Switch, BrowserRouter } from "react-router-dom";
import { compose } from "redux";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import LoadingPage from "containers/LoadingPage";
import ApiService from "api/Routes";
import { autoLoginRequest, getUserRequest } from "api/actions/UserActions";
import { api } from "api/Sagas";

import LoginPage from "containers/LoginPage";
import UserArea from "containers/UserArea";
import SignupPage from "containers/SignupPage";
import ActivatePage from "containers/ActivatePage";
import ResizeHandler from "components/ResizeHandler";

function Logout() {
  localStorage.removeItem("token");
  api.removeToken();
  document.location.pathname = "/";

  return <LoadingPage isVisible />;
}

if (localStorage.token) {
  api.setToken(localStorage.token);
}

class App extends React.Component {

  firstLogin = false;

  state = {
    autoLogin: false,
    loading: false
  };

  constructor(props) {
    super(props);
    if (localStorage.token) {
      this.state.autoLogin = true;
      this.firstLogin = true;
      props.postAutoLogin();
    }
  }

  componentWillReceiveProps(props) {
    const state = {};
    if (props.user.fetching) {
      state.loading = true;
    }

    if (!props.user.fetching && props.user !== this.props.user) {
      state.loading = false;
      if (!this.firstLogin && this.state.autoLogin) {
        state.autoLogin = false;
      } else if (this.firstLogin) {
        this.firstLogin = false;
      }
    }
    this.setState(state);
  }

  render() {
    const { autoLogin, loading } = this.state;
    const { fetching, error, data } = this.props.user;

    return (
      <BrowserRouter basename="/app">
        <ResizeHandler>
            <LoadingPage isVisible={fetching || loading} />
            { !fetching && (
              (localStorage.token && !error)
                ? <Switch><Route path="/" render={props => <UserArea logout={Logout} {...props} />} /></Switch>
                : (
                  <Switch>
                    <Route path="/signup" render={props => <SignupPage error={!autoLogin && error} {...props} />} />
                    <Route path="/activate/:token" render={props => <ActivatePage {...props} />} />
                    <Route path="/" exact render={props => <LoginPage error={!autoLogin && error} {...props} />} />
                  </Switch>
                )
            ) }
        </ResizeHandler>
      </BrowserRouter>
    );
  }
}

App.propTypes = {
  postAutoLogin: PropTypes.func.isRequired
};

function mapStateToProps(mapState) {
  const state = mapState.toJS();
  const { user } = state.api;
  return {
    user
  };
}

function mapDispatchToProps(dispatch) {
  return {
    postAutoLogin: () => dispatch(autoLoginRequest())
  };
}

const withConnect = connect(mapStateToProps, mapDispatchToProps, null, { pure: false, withRef: true });
export default compose(withConnect)(App);
