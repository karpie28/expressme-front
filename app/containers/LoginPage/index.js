/**
 *
 * LoginPage
 *
 */

import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import { compose } from "redux";
import Input from "components/Input";
import Button from "components/Button";
import * as mouths from "images/mouths";
import style from "./style.less";
import { loginRequest } from "api/actions/UserActions";

export class LoginPage extends React.Component { // eslint-disable-line react/prefer-stateless-function
  state = {
    password: "",
    email: "",
    error: false,
    signUp: false
  };

  handleSubmit = (e) => {
    e.preventDefault();
    const { email, password } = this.state;
    this.props.login(email, password);
  };

  componentWillReceiveProps(props){
    if (props.error !== this.props.error) {
      this.setState({ error: props.error });
    }
  }

  render() {
    const { error, signUp } = this.state;

    const errorClasses = [
      style.login__error,
      error && style["login__error--visible"]
    ].filter(Boolean).join(" ");

    return (
      <div className={style.login}>
        <Helmet>
          <title>Login - ExpressMe</title>
          <meta name="description" content="Description of LoginPage" />
        </Helmet>
        <div className={style.login__header}>
          <h1>ExpressMe</h1>
          <h3>Social media that can feel!</h3>
        </div>
        <div className={style.login__mouths}>
          { Object.values(mouths).reverse().map((mouth, key) => <img key={key} src={mouth} />) }
        </div>
        <span className={errorClasses}>
          An error occurred!<br />
          Please make sure you have entered the correct data and try again.
        </span>
        <form className={style.login__form} onSubmit={this.handleSubmit}>
          <Input
            className={style.input}
            type="email"
            placeholder="Email"
            value={this.state.email}
            onChange={email => this.setState({ email })}
            required
            input={{ required: true }}
          />
          <Input
            className={style.input}
            type="password"
            placeholder="Password"
            value={this.state.password}
            onChange={password => this.setState({ password })}
            required
            input={{ required: true }}
          />
          <Button className={style.button} href="form.submit">Sign In</Button>
        </form>
        <span>Don't have an account yet? <Button href="/signup" className={style.button} noMargin>Sign Up</Button></span>
      </div>
    );
  }
}

LoginPage.propTypes = {
  login: PropTypes.func.isRequired,
  error: PropTypes.object
};


function mapDispatchToProps(dispatch) {
  return {
    login: (email, password) => dispatch(loginRequest({ email, password }))
  };
}

function mapStateToProps(mapState) {
  const state = mapState.toJS();
  const { user } = state.api;
  return user;
}

const withConnect = connect(null, mapDispatchToProps);

export default compose(
  withConnect,
)(LoginPage);
