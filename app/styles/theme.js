/**
 * NEVER directly export the palette! It should be used only to define components-related colors!
 *
 * Use your creativity or http://chir.ag/projects/name-that-color to create a name of color
 * in the palette. Don't limit yourself!
 */

const palette = {
  white: "#FFF",
  desertStorm: "#F3F3F2",
  gondola: "#261416",
  creamCan: "#F4CB55",
  sushi: "#88BA46",
  mandy: "#DE4B5B",
  desertTransparent: "rgba(243, 243, 242, 0.17)"
};

/*
  Standard sizes
 */

const sizes = {
  xxsmall: 4,
  xsmall: 8,
  small: 16,
  medium: 24,
  large: 48,
  xlarge: 96,
  xxlarge: 192
};

const fonts = {
  fontFamily: "\"Muli\", \"Segoe UI\", Tahoma, \"Arial Black\", \"Impact\", sans-serif",
  baseSize: `${sizes.small}px`,
  baseColor: palette.gondola
};

/*
  General variables are merged with each variables group and they're available until they aren't overridden
 */


const general = {
  background: palette.creamCan,
  text: palette.gondola
};

/* *******************************************
  Elements variables
 */

const loginPage = {
  text: palette.white
};

const blankPage = {
  background: palette.creamCan
};

const inputComponent = {
  background: palette.desertTransparent,
  active: palette.gondola,
  padding: "30px",
  borderRadius: "8px",
  text: palette.white,
  placeholder: palette.gondola,
  disabled: palette.gondola
};

/* *******************************************
  Exported variables
 */

let exported = {
  loginPage,
  blankPage,
  inputComponent
};

exported = Object.keys(exported).reduce((result, key) => {
  result[key] = Object.assign({}, general, exported[key]); // eslint-disable-line no-param-reassign
  return result;
}, {});

const result = {
  sizes,
  fonts,
  general,
  ...exported
};

module.exports = {
  install: (less, pluginManager, functions) => {
    functions.add("theme", (...args) => {
      const screen = args[1] ? args[0] && args[0].value : "general";
      const variable = args[1] ? args[1].value : args[0] && args[0].value;
      return result[screen] && result[screen][variable] || false;
    });
    Object.keys(result).forEach(key => {
      functions.add(key, variable => variable.value && result[key][variable.value] || false);
    });
  }
};
